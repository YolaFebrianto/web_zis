<?php
class M_halaman extends CI_Model{
	function get_all_halaman(){
		$hsl=$this->db->query("SELECT tbl_halaman.* FROM tbl_halaman ORDER BY halaman_id DESC");
		return $hsl;
	}
	function simpan_halaman($judul,$isi,$slug){
		$hsl=$this->db->query("INSERT INTO tbl_halaman(halaman_judul,halaman_isi,halaman_slug) VALUES ('$judul','$isi','$slug')");
		return $hsl;
	}
	function get_halaman_by_kode($kode){
		$hsl=$this->db->query("SELECT tbl_halaman.* FROM tbl_halaman WHERE halaman_id='$kode'");
		return $hsl;
	}
	function get_halaman_by_slug($slug){
		$hsl=$this->db->query("SELECT tbl_halaman.* FROM tbl_halaman WHERE halaman_slug='$slug'");
		return $hsl;
	}
	function update_halaman($halaman_id,$judul,$isi,$slug){
		$hsl=$this->db->query("UPDATE tbl_halaman SET halaman_judul='$judul',halaman_isi='$isi',halaman_slug='$slug' WHERE halaman_id='$halaman_id'");
		return $hsl;
	}
	function hapus_halaman($kode){
		$hsl=$this->db->query("DELETE FROM tbl_halaman WHERE halaman_id='$kode'");
		return $hsl;
	}
}
