<?php
class M_donasi extends CI_Model{
	function get_all_donasi(){
		$hsl=$this->db->query("SELECT * FROM tbl_donasi");
		return $hsl;	
	}

	function get_donasi_by_kode($id){
		$hsl=$this->db->query("SELECT * FROM tbl_donasi WHERE donasi_id='$id'");
		return $hsl;	
	}

	function simpan_donasi($nama_donasi,$jumlah_donasi,$gambar){
		$hsl=$this->db->query("INSERT INTO tbl_donasi (nama_donasi,jumlah_donasi,gambar) VALUES ('$nama_donasi','$jumlah_donasi','$gambar')");
		return $hsl;
	}

	function simpan_donasi_tanpa_gambar($nama_donasi,$jumlah_donasi){
		$hsl=$this->db->query("INSERT INTO tbl_donasi (nama_donasi,jumlah_donasi) VALUES ('$nama_donasi','$jumlah_donasi')");
		return $hsl;
	}

	function update_donasi($nama_donasi,$jumlah_donasi,$gambar,$id){
		$hsl=$this->db->query("UPDATE tbl_donasi set nama_donasi='$nama_donasi',jumlah_donasi='$jumlah_donasi',gambar='$gambar' WHERE donasi_id='$id'");
		return $hsl;
	}

	function update_donasi_tanpa_gambar($nama_donasi,$jumlah_donasi,$id){
		$hsl=$this->db->query("UPDATE tbl_donasi set nama_donasi='$nama_donasi',jumlah_donasi='$jumlah_donasi' WHERE donasi_id='$id'");
		return $hsl;
	}

	function hapus_donasi($id){
		$hsl=$this->db->query("DELETE FROM tbl_donasi WHERE donasi_id='$id'");
		return $hsl;
	}
}