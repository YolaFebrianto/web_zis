<?php
class M_infaq extends CI_Model{
	function get_all_infaq(){
		$hsl=$this->db->query("SELECT * FROM tbl_infaq");
		return $hsl;	
	}

	function get_infaq_by_kode($id){
		$hsl=$this->db->query("SELECT * FROM tbl_infaq WHERE infaq_id='$id'");
		return $hsl;	
	}

	function simpan_infaq($nama_infaq,$jumlah_infaq,$gambar){
		$hsl=$this->db->query("INSERT INTO tbl_infaq (nama_infaq,jumlah_infaq,gambar) VALUES ('$nama_infaq','$jumlah_infaq','$gambar')");
		return $hsl;
	}

	function simpan_infaq_tanpa_gambar($nama_infaq,$jumlah_infaq){
		$hsl=$this->db->query("INSERT INTO tbl_infaq (nama_infaq,jumlah_infaq) VALUES ('$nama_infaq','$jumlah_infaq')");
		return $hsl;
	}

	function update_infaq($nama_infaq,$jumlah_infaq,$gambar,$id){
		$hsl=$this->db->query("UPDATE tbl_infaq SET nama_infaq='$nama_infaq',jumlah_infaq='$jumlah_infaq',gambar='$gambar' WHERE infaq_id='$id'");
		return $hsl;
	}

	function update_infaq_tanpa_gambar($nama_infaq,$jumlah_infaq,$id){
		$hsl=$this->db->query("UPDATE tbl_infaq SET nama_infaq='$nama_infaq',jumlah_infaq='$jumlah_infaq' WHERE infaq_id='$id'");
		return $hsl;
	}

	function hapus_infaq($id){
		$hsl=$this->db->query("DELETE FROM tbl_infaq WHERE infaq_id='$id'");
		return $hsl;
	}
}