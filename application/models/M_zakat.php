<?php
class M_zakat extends CI_Model{
	function get_all_zakat(){
		$hsl=$this->db->query("SELECT * FROM tbl_zakat");
		return $hsl;	
	}

	function get_zakat_by_kode($id){
		$hsl=$this->db->query("SELECT * FROM tbl_zakat WHERE zakat_id='$id'");
		return $hsl;	
	}

	function simpan_zakat($nama_zakat,$jumlah_zakat,$gambar){
		$hsl=$this->db->query("INSERT INTO tbl_zakat (nama_zakat,jumlah_zakat,gambar) VALUES ('$nama_zakat','$jumlah_zakat','$gambar')");
		return $hsl;
	}

	function simpan_zakat_tanpa_gambar($nama_zakat,$jumlah_zakat){
		$hsl=$this->db->query("INSERT INTO tbl_zakat (nama_zakat,jumlah_zakat) VALUES ('$nama_zakat','$jumlah_zakat')");
		return $hsl;
	}

	function update_zakat($nama_zakat,$jumlah_zakat,$gambar,$id){
		$hsl=$this->db->query("UPDATE tbl_zakat SET nama_zakat='$nama_zakat',jumlah_zakat='$jumlah_zakat',gambar='$gambar' WHERE zakat_id='$id'");
		return $hsl;
	}

	function update_zakat_tanpa_gambar($nama_zakat,$jumlah_zakat,$id){
		$hsl=$this->db->query("UPDATE tbl_zakat SET nama_zakat='$nama_zakat',jumlah_zakat='$jumlah_zakat' WHERE zakat_id='$id'");
		return $hsl;
	}

	function hapus_zakat($id){
		$hsl=$this->db->query("DELETE FROM tbl_zakat WHERE zakat_id='$id'");
		return $hsl;
	}

}