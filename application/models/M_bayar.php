<?php
class M_bayar extends CI_Model{
    function get_all_bayar(){
        $hsl=$this->db->query("SELECT * FROM tbl_bayar");
        return $hsl;    
    }
    function get_sum_bayar_id_program($id){
        $hsl=$this->db->query("SELECT SUM(bayar_nilai) as total FROM tbl_bayar WHERE bayar_id_program='$id' AND bayar_status='1'");
        return $hsl;    
    }
    function get_sum_bayar(){
        $hsl=$this->db->query("SELECT SUM(bayar_nilai) as total FROM tbl_bayar WHERE bayar_status='1'");
        return $hsl;    
    }
    function bayar($nama,$nilai,$bukti){
        $hasil=$this->db->query("INSERT INTO tbl_bayar(bayar_nama,bayar_nilai,bayar_bukti) VALUES('$nama','$nilai','$bukti')");
        return $hasil;
    }
    function bayar_tanpa_gambar($nama,$nilai){
        $hasil=$this->db->query("INSERT INTO tbl_bayar(bayar_nama,bayar_nilai) VALUES('$nama','$nilai')");
        return $hasil;
    }
    function bayar_program($nama,$nilai,$bukti,$jenis_program,$id_program){
        $hasil=$this->db->query("INSERT INTO tbl_bayar(bayar_nama, bayar_nilai, bayar_bukti, bayar_jenis_program, bayar_id_program) VALUES('$nama', '$nilai','$bukti', '$jenis_program', '$id_program')");
        return $hasil;
    }
    function bayar_program_tanpa_gambar($nama,$nilai,$jenis_program,$id_program){
        $hasil=$this->db->query("INSERT INTO tbl_bayar(bayar_nama, bayar_nilai, bayar_jenis_program, bayar_id_program) VALUES('$nama', '$nilai', '$jenis_program', '$id_program')");
        return $hasil;
    }
    function publish($id){
        $hasil=$this->db->query("UPDATE tbl_bayar SET bayar_status='1' WHERE bayar_id='$id'");
        return $hasil;
    }
    function hapus($id){
        $hsl=$this->db->query("DELETE FROM tbl_bayar WHERE bayar_id='$id'");
        return $hsl;
    }
}