<?php
	function limit_words($string, $word_limit){
		$words = explode(" ",$string);
		return implode(" ",array_splice($words,0,$word_limit));
	}
	function get_web_setting(){
		$CI = get_instance();
		$CI->load->model('M_setting');
		$data = $CI->M_setting->get()->row_array();
		return $data;
	}
	function get_admin_js(){
		$js = '<!-- jQuery 2.2.3 -->
		<script src="'.base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'.'"></script>
		<!-- DataTables -->
		<script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js'.'"></script>
		<script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'.'"></script>
		<script>
		  $(function () {
		    $("#example1").DataTable();
		    $("#example2").DataTable({
		      "paging": true,
		      "lengthChange": false,
		      "searching": true,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false
		    });
		  });
		</script>';
		echo $js;
	}
	function get_sum_bayar($id=0){
		$hsl=0;
		$CI = get_instance();
		$CI->load->model('M_bayar');
		if ($id>0) {
			$data = $CI->M_bayar->get_sum_bayar_id_program($id)->row_array();
			$hsl = @$data['total'];
		} else {
			$data = $CI->M_bayar->get_sum_bayar()->row_array();
			$hsl = @$data['total'];
		}
		if (empty($hsl)) {
			$hsl=0;
		}
        return $hsl;    
    }
?>