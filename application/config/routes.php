<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['administrator']='admin/login';
$route['administrator/logout']='admin/login/logout';
$route['artikel']='berita';
$route['artikel']='berita/index';
$route['artikel/(:any)']='berita/detail/$1';
$route['aksi']='program';
$route['aksi']='program/index';
$route['aksi/(:any)/(:any)']='program/$1/$2';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
