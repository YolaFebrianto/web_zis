 <?php
        $dataSettinganWeb = get_web_setting();
    ?>
 <footer class="ftco-footer ftco-section">
      <div class="container">
        <div class="row">
            <div class="mouse">
                        <a href="#" class="mouse-icon">
                            <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
                        </a>
                    </div>
        </div>
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2"><?php echo @$dataSettinganWeb['nama']; ?></h2>
              <p><?php echo @$dataSettinganWeb['alamat'].', '.@$dataSettinganWeb['kabupaten'].', '.@$dataSettinganWeb['propinsi']; ?></p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="<?php echo base_url();?>" class="py-2 d-block">Home</a></li>
                <li><a href="<?php echo base_url('home/profil');?>" class="py-2 d-block">Profil</a></li>
                <li><a href="<?php echo base_url('home/visi_misi');?>" class="py-2 d-block">Visi Misi</a></li>
                <li><a href="<?php echo base_url('home/program');?>" class="py-2 d-block">Program</a></li>
                <li><a href="<?php echo base_url('home/kalkulator');?>" class="py-2 d-block">Kalkulator Zakat</a></li>
              </ul>
            </div>
          </div>
          <?php
            $this->load->model('M_tulisan');
            $list_berita=$this->M_tulisan->get_berita_home(6);
            $list_kategori=$this->db->get('tbl_kategori');
          ?>
          <div class="col-md-4">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Berita Terbaru</h2>
              <div class="d-flex">
                  <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                    <?php foreach ($list_berita->result() as $row): ?>
                    <li><a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>" class="py-2 d-block"><?php echo limit_words($row->tulisan_judul,3).'...';?></a></li>
                    <?php endforeach;?>
                  </ul>
                  <ul class="list-unstyled">
                    <?php foreach ($list_kategori->result() as $row2) :?>
                    <li><a href="<?php echo site_url('berita/kategori/'.str_replace(" ","-",$row2->kategori_nama));?>" class="py-2 d-block"><?php echo $row2->kategori_nama;?></a></li>
                    <?php endforeach;?>
                  </ul>
                </div>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Kontak</h2>
                <div class="block-23 mb-3">
                  <ul>
                    <li><span class="icon icon-map-marker"></span><span class="text"><?php echo @$dataSettinganWeb['alamat'].', '.@$dataSettinganWeb['kabupaten'].', '.@$dataSettinganWeb['propinsi']; ?></span></li>
                    <li><a href="#"><span class="icon icon-phone"></span><span class="text"><?php echo @$dataSettinganWeb['telepon']; ?></span></a></li>
                    <li><a href="#"><span class="icon icon-envelope"></span><span class="text"><?php echo @$dataSettinganWeb['email']; ?></span></a></li>
                  </ul>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
          </div>
        </div>
      </div>
    </footer>
    
  




  <script src="<?php echo base_url() ?>minishop/js/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/jquery.stellar.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/aos.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/jquery.animateNumber.min.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?php echo base_url() ?>minishop/js/google-map.js"></script>
  <script src="<?php echo base_url() ?>minishop/js/main.js"></script>
    
  </body>
</html>