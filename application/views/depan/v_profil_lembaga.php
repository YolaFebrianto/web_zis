<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url() ?>minishop/images/bg_6.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Tentang Kami</span></p>
        <h1 class="mb-0 bread">Profil Lembaga</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12 py-md-5 wrap-about pb-md-5 ftco-animate">
                <div class="heading-section-bold mb-4 mt-md-5">
                    <div class="ml-md-0">
                        <h2 class="mb-4">Lazismu</h2>
                    </div>
                </div>
                <div class="pb-md-5 pb-4" style="text-align: justify;">
                    <?php echo @$isi['halaman_isi']; ?>
                </div>
            </div>
        </div>
    </div>
</section>   