<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url() ?>minishop/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Tentang Kami</span></p>
            <h1 class="mb-0 bread">Program</h1>
          </div>
        </div>
      </div>
    </div>
    <div>
      <?php echo $this->session->flashdata('msg');?>
    </div>
    		<div class="row mt-5">
          <div class="col-md-12 nav-link-wrap">
            <div class="nav nav-pills d-flex text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link ftco-animate active mr-lg-1" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Zakat</a>

              <a class="nav-link ftco-animate mr-lg-1" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Donasi</a>


              <a class="nav-link ftco-animate" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">Infaq</a>

            </div>
          </div>
          <div class="col-md-12 tab-wrap">
            <div class="tab-content bg-light" id="v-pills-tabContent">

              <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="day-1-tab">
              	<div class="container">
            <div class="row">
                <?php foreach ($data1->result_array() as $i) :
                     $id=$i['zakat_id'];
                     $nama=$i['nama_zakat'];
                     // $jumlah=$i['jumlah_zakat'];
                     $gambar=$i['gambar'];
                     $jumlah = get_sum_bayar($id);
                  ?>
                <div class="col-sm-12 col-md-6 col-lg-3 ftco-animate d-flex">
                    <div class="product d-flex flex-column">
                        <a href="#" class="img-prod"><img class="img-fluid" src="<?php echo base_url().'assets/images/'.$gambar;?>" alt="Colorlib Template">
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3">
                            <h3><a href="#"><?php echo $nama; ?></a></h3>
                            <div class="pricing">
                                <p class="price"><span style="color:red;">Rp. <?php echo number_format($jumlah,2,',','.') ; ?>&nbsp;&nbsp;</span><span style="font-size:10px;">terkumpul</span></p>
                            </div>
                            <p class="bottom-area d-flex px-3">
                                <a href="<?php echo site_url('aksi/aksizakat/'.$id);?>" class="add-to-cart text-center py-2 mr-1"><span>Donate now <i class="ion-ios-add ml-1"></i></span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <?php 
           
            endforeach;?>
            </div>
        </div>
              </div>

              <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-day-2-tab">
              <div class="container">
            <div class="row">
                <?php foreach ($data->result_array() as $i) :
                     $id=$i['donasi_id'];
                     $nama=$i['nama_donasi'];
                     // $jumlah=$i['jumlah_donasi'];
                     $gambar=$i['gambar'];
                     $jumlah = get_sum_bayar($id);
                  ?>
                <div class="col-sm-12 col-md-6 col-lg-3 ftco-animate d-flex">
                    <div class="product d-flex flex-column">
                        <a href="#" class="img-prod"><img class="img-fluid" src="<?php echo base_url().'assets/images/'.$gambar;?>" alt="Colorlib Template">
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3">
                            <h3><a href="#"><?php echo $nama; ?></a></h3>
                            <div class="pricing">
                                <p class="price"><span style="color:red;">Rp. <?php echo number_format($jumlah,2,',','.') ; ?>&nbsp;&nbsp;</span><span style="font-size:10px;">terkumpul</span></p>
                            </div>
                            <p class="bottom-area d-flex px-3">
                                <a href="<?php echo site_url('aksi/aksidonasi/'.$id);?>" class="add-to-cart text-center py-2 mr-1"><span>Donate now <i class="ion-ios-add ml-1"></i></span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <?php 
           
            endforeach;?>
            </div>
        </div>
        </div>
              <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-day-3-tab">
              	<div class="container">
            <div class="row">
                <?php foreach ($data2->result_array() as $i) :
                     $id=$i['infaq_id'];
                     $nama=$i['nama_infaq'];
                     // $jumlah=$i['jumlah_infaq'];
                     $gambar=$i['gambar'];
                     $jumlah = get_sum_bayar($id);
                  ?>
                <div class="col-sm-12 col-md-6 col-lg-3 ftco-animate d-flex">
                    <div class="product d-flex flex-column">
                        <a href="#" class="img-prod"><img class="img-fluid" src="<?php echo base_url().'assets/images/'.$gambar;?>" alt="Colorlib Template">
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3">
                            <h3><a href="#"><?php echo $nama; ?></a></h3>
                            <div class="pricing">
                                <p class="price"><span style="color:red;">Rp. <?php echo number_format($jumlah,2,',','.') ; ?>&nbsp;&nbsp;</span><span style="font-size:10px;">terkumpul</span></p>
                            </div>
                            <p class="bottom-area d-flex px-3">
                                <a href="<?php echo site_url('aksi/aksiinfaq/'.$id);?>" class="add-to-cart text-center py-2 mr-1"><span>Donate now <i class="ion-ios-add ml-1"></i></span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <?php 
           
            endforeach;?>
            </div>
        </div>
        </div>