    <section id="home-section" class="hero">
          <div class="home-slider owl-carousel">
          <div class="slider-item js-fullheight">
            <div class="overlay"></div>
            <div class="container-fluid p-0">
              <div class="row d-md-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                <img class="one-third order-md-last img-fluid" src="<?php echo base_url() ?>templates/img/icons/bg1.jpg" alt="">
                  <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                    <div class="text">
                        <span class="subheading">Bantu Sosial</span>
                        <div class="horizontal">
                            <h1 class="mb-4 mt-3">Bantu Adik adik kita yang kurang mampu</h1>
                            <p><a href="<?php echo base_url('home/program') ?>" class="btn-custom">Donate Now</a></p>
                          </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="slider-item js-fullheight">
            <div class="overlay"></div>
            <div class="container-fluid p-0">
              <div class="row d-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                <img class="one-third order-md-last img-fluid" src="<?php echo base_url() ?>templates/img/icons/bg2.png" alt="">
                  <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                    <div class="text">
                        <span class="subheading">bantu Operasi Ambulance Desa</span>
                        <div class="horizontal">
                            <p><a href="<?php echo base_url('home/program') ?>" class="btn-custom">Donate Now</a></p>
                          </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </section>
    <section class="ftco-section ftco-no-pt ftco-no-pb">
            <div class="container">
                <div class="row no-gutters ftco-services">
          <div class="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-4 py-md-5">
              <div class="icon d-flex justify-content-center align-items-center mb-4">
                    <span class="flaticon-customer-service"></span>
              </div>
              <?php
                $query=$this->db->query("SELECT * FROM tbl_pengunjung WHERE pengunjung_perangkat='Chrome'");
                $jml=$query->num_rows();
            ?>
              <div class="media-body">
                <h3 class="heading">Pengunjung</h3>
                <p style="font-size: 40px; color: orange; font-weight: 700;"> <?php echo $jml;?></p>
              </div>
            </div>      
          </div>
          <div class="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-4 py-md-5">
              <div class="icon d-flex justify-content-center align-items-center mb-4">
                    <span class="flaticon-payment-security"></span>
              </div>
              <?php
                // $query=$this->db->query("SELECT SUM(a.jumlah) AS saldo FROM (
                //   SELECT SUM(z.jumlah_zakat) AS jumlah FROM tbl_zakat z
                //   UNION 
                //   SELECT SUM(i.jumlah_infaq) AS jumlah FROM tbl_infaq i
                //   UNION 
                //   SELECT SUM(s.jumlah_donasi) AS jumlah FROM tbl_donasi s
                // ) AS a");
                // $jml=$query->row();
                // // var_dump($jml->saldo);
                // $saldo = @$jml->saldo;
                $saldo = get_sum_bayar();
                if(!empty(@$saldo) AND $saldo>0){
                    $saldo = number_format($saldo,2,',','.');
                }
            ?>
              <div class="media-body">
                <h3 class="heading">Total Donasi</h3>
               <p style="font-size: 40px; color: orange; font-weight: 700;"> Rp.<?php echo @$saldo; ?></p>
              </div>
            </div>    
          </div>
          <div class="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services p-4 py-md-5">
              <div class="icon d-flex justify-content-center align-items-center mb-4">
                    <span class="flaticon-heart-box"></span>
              </div>
               <?php
                  $query=$this->db->query("SELECT * FROM tbl_pengguna");
                  $jml=$query->num_rows();
            ?>
              <div class="media-body">
                <h3 class="heading">Donatur</h3>
                <p style="font-size: 40px; color: orange; font-weight: 700;"> <?php echo $jml;?></p>
              </div>
            </div>      
          </div>
        </div>
            </div>
        </section>
    <section class="ftco-section bg-light">
        <div class="container">
                <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Program Pilihan</h2>
          </div>
        </div>          
        </div>
        <div class="container">
            <div class="row">
                <?php foreach ($data->result_array() as $i) :
                     $id=$i['zakat_id'];
                     $nama=$i['nama_zakat'];
                     // $jumlah=$i['jumlah_donasi'];
                     $gambar=$i['gambar'];
                     $jumlah = get_sum_bayar($id);
                  ?>
                <div class="col-sm-12 col-md-6 col-lg-3 ftco-animate d-flex">
                    <div class="product d-flex flex-column">
                        <a href="#" class="img-prod"><img class="img-fluid" src="<?php echo base_url().'assets/images/'.$gambar;?>" alt="Colorlib Template">
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3">
                            <h3><a href="#"><?php echo $nama; ?></a></h3>
                            <div class="pricing">
                                <p class="price"><span style="color:red;">Rp. <?php echo number_format($jumlah,2,',','.') ; ?>&nbsp;&nbsp;</span><span style="font-size:10px;">terkumpul</span></p>
                            </div>
                            <p class="bottom-area d-flex px-3">
                                <a href="<?php echo site_url('aksi/aksizakat/'.$nama);?>" class="add-to-cart text-center py-2 mr-1"><span>Donate now <i class="ion-ios-add ml-1"></i></span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <?php 
            endforeach;?>
            </div>
        </div>
    </section>
     <section class="ftco-section ftco-deal bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo base_url() ?>minishop/images/gambar10.png" class="img-fluid" alt="" height="100%" width="100%">
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 order-lg-last ftco-animate">
            <?php
                  $no=0;
                  foreach ($data2->result_array() as $i) :
                     $no++;
                     $tulisan_id=$i['tulisan_id'];
                     $tulisan_judul=$i['tulisan_judul'];
                     $tulisan_isi=$i['tulisan_isi'];
                     $tulisan_tanggal=$i['tanggal'];
                     $tulisan_author=$i['tulisan_author'];
                     $tulisan_gambar=$i['tulisan_gambar'];
                     $tulisan_slug=$i['tulisan_slug'];
                     $tulisan_views=$i['tulisan_views'];
                     $kategori_id=$i['tulisan_kategori_id'];
                     $kategori_nama=$i['tulisan_kategori_nama'];

                  ?>
              <div class="row">
                  <div class="col-md-12 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch d-md-flex">
                      <a href="blog-single.html" class="block-20" style="background-image: url('<?php echo base_url().'assets/images/'.$tulisan_gambar;?>');">
                      </a>
                      <div class="text d-block pl-md-4">
                        <div class="meta mb-3">
                          <div><a href="#"><?php echo $tulisan_tanggal;?></a></div>
                          <div><a href="#"><?php echo $tulisan_author;?></a></div>
                          <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <h3 class="heading"><a href="<?php echo site_url('artikel/'.$tulisan_slug);?>"><?php echo $tulisan_judul;?></a></h3>
                        <p><?= substr(strip_tags($tulisan_isi), 0, 150) ?></p>
                        <p><a href="<?php echo site_url('artikel/'.$tulisan_slug);?>" class="btn btn-primary py-2 px-3">Read more</a></p>
                      </div>
                    </div>
                  </div>  
                </div>
              <?php endforeach; ?>

                        <div class="row mt-2">
                </div>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar ftco-animate">
            <div class="sidebar-box ftco-animate">
              <h3 class="heading">Recent Blog</h3>

              <?php 

              foreach ($data2->result_array() as $i) :
                $no++;
                     $tulisan_id=$i['tulisan_id'];
                     $tulisan_judul=$i['tulisan_judul'];
                     $tulisan_isi=$i['tulisan_isi'];
                     $tulisan_tanggal=$i['tanggal'];
                     $tulisan_author=$i['tulisan_author'];
                     $tulisan_gambar=$i['tulisan_gambar'];
                      $tulisan_slug=$i['tulisan_slug'];
                     $tulisan_views=$i['tulisan_views'];
                     $kategori_id=$i['tulisan_kategori_id'];
                     $kategori_nama=$i['tulisan_kategori_nama'];

               ?>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(<?php echo base_url().'assets/images/'.$tulisan_gambar;?>);"></a>
                <div class="text">
                  <h3 class="heading-1"><a href="<?php echo site_url('artikel/'.$tulisan_slug);?>"><?= substr(strip_tags($tulisan_isi), 0, 50) ?></a></h3>
                  <div class="meta">
                    <div><span class="icon-calendar"></span> <?php echo $tulisan_tanggal;?></div>
                    <div><span class="icon-person"></span> <?php echo $tulisan_author;?></div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
            
              
            </div>
          </div>
        </div>
      </div>
    </section> <!-- .section -->


    
    

    
   