    <?php
        $dataSettinganWeb = get_web_setting();
    ?>
    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-body footer mt-5 pt-5 px-0 wow fadeIn" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Tentang Kami</h3>
                    <p lass="mb-2">LAZISMU adalah lembaga amil zakat nasional dengan SK Menag No. 730 Tahun 2016, yang berkhidmat dalam pemberdayaan masyarakat, melalui pendayagunaan dana zakat, infaq dan dana kedermawanan lainnya baik dari perseorangan, lembaga, perusahaan dan instansi lainnya.</p>
                    <p class="mb-2"><i class="fa fa-map-marker-alt text-primary me-3"></i><?php echo @$dataSettinganWeb['alamat'].', '.@$dataSettinganWeb['kabupaten'].', '.@$dataSettinganWeb['propinsi']; ?></p>
                    <p class="mb-2"><i class="fa fa-phone-alt text-primary me-3"></i><?php echo @$dataSettinganWeb['telepon']; ?></p>
                    <p class="mb-2"><i class="fa fa-envelope text-primary me-3"></i><?php echo @$dataSettinganWeb['email']; ?></p>
                    <div class="d-flex pt-2">
                        <a class="btn btn-square btn-outline-body me-1" href=""><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-square btn-outline-body me-1" href=""><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-square btn-outline-body me-1" href=""><i class="fab fa-youtube"></i></a>
                        <a class="btn btn-square btn-outline-body me-0" href=""><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Rekening Donasi</h3>
                    <a class="btn btn-link">Bank Syariah Indonesia 711 717 0017
                    a.n. Lazismu Kab Sidoarjo</a>
                    <a class="btn btn-link">Bank Syariah Indonesia 711 717 0017
                    a.n. Lazismu Kab Sidoarjo</a>
                    <a class="btn btn-link">BRI 004 401 002 416 308
                    a.n. Infaq Lazismu Kab sidoarjo</a>
                    <a class="btn btn-link">Bank Muamalat 713 001 0157
                    a.n. Lazis Muhammadiyah Kab sidoarjo</a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Rekening Zakat</h3>
                    <a class="btn btn-link" >Bank Syariah Indonesia <?php echo @$dataSettinganWeb['norek']; ?>
                        a.n. Zakat Lazismu Kab Sidoarjo</a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 class="text-light mb-4">Donasi Instan</h3>
                    <img src="<?php echo base_url() ?>templates/img/footer.jpg" width="100%" height="30%">
                    <p style="text-align: justify;">Dana yang didonasikan melalui Lazismu Lumajang bukan bersumber dan bukan untuk tujuan pencucian uang (money laundry), termasuk terorisme maupun tindak kejahatan lainnya.</p>
                </div>
            </div>
        </div>
        <div class="container-fluid copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                        &copy; <a href="#"><?php echo @$dataSettinganWeb['nama']; ?></a>, All Right Reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/easing/easing.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/waypoints/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/counterup/counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/tempusdominus/js/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/tempusdominus/js/moment-timezone.min.js"></script>
    <script src="<?php echo base_url(); ?>templates/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

    <!-- Template Javascript -->
    <script src="<?php echo base_url(); ?>templates/js/main.js"></script>
</body>

</html>