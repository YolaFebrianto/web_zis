<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url() ?>minishop/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Program</span></p>
            <h1 class="mb-0 bread">Infaq</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 ftco-animate">
            <a href="<?php echo base_url().'assets/images/'.$infaq['gambar'];?>" class="image-popup prod-img-bg"><img src="<?php echo base_url().'assets/images/'.$infaq['gambar'];?>" class="img-fluid" alt="Colorlib Template" width="100%"></a>
          </div>
          <div class="col-lg-6 product-details pl-md-5 ftco-animate">
            <h3><?php echo @$infaq['nama_infaq']; ?></h3>
            <p class="price"><span>Rp. <?php echo number_format(@$nilai['total'],2,',','.'); ?> <br>Total Infaq terkumpul</span></p>
            <?php echo $this->session->flashdata('msg');?>
            <form action="<?php echo site_url('program/bayar');?>" method="post" enctype="multipart/form-data">
              <div class="row mt-4">
                <div class="col-md-6">
                  <div class="form-group d-flex">
                    <div class="select-wrap">
                      
                    </div>
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="input-group col-md-6 d-flex mb-3">
                  <span class="input-group-btn mr-2">
                      <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                       <i class="ion-ios-remove"></i>
                      </button>
                    </span>
                  <input type="text" id="quantity" name="bayar_nilai" class="quantity form-control input-number" value="10000" min="1" max="100">
                  <span class="input-group-btn ml-2">
                      <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                         <i class="ion-ios-add"></i>
                     </button>
                  </span>
                </div>
                <div class="w-100"></div>
                  <input type="hidden" name="bayar_id_program" value="<?php echo @$infaq['infaq_id']; ?>">
                  <input type="hidden" name="bayar_jenis_program" value="INFAQ">
                  <fieldset>
                      <legend style="font-size:14px;line-height:18px;font-style:italic;">Harap isi nama dan upload bukti pembayaran, saat melakukan pembayaran zakat:</legend>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Nama </label>
                              <input type="text" class="form-control" placeholder="Nama" name="bayar_nama" required id="ph_nama" value="Hamba Allah">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label>Bukti Pembayaran</label>
                              <input type="file" name="bayar_bukti" class="form-control">
                          </div>
                        </div>
                      </div>
                  </fieldset>
                  <button type="submit" class="btn btn-primary" style="display:none;" id="btn-kirim">Kirim</button>
              </div>
              <p>
                <a href="#" class="btn btn-primary py-3 px-5" onclick="kirimForm()">Donasi Sekarang</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </section>
<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
<script type="text/javascript">
  function kirimForm(){
    $('#btn-kirim').click();
  }
</script>