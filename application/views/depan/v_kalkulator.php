<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url() ?>minishop/images/bg_6.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Layanan</span></p>
        <h1 class="mb-0 bread">Kalkulator Zakat</h1>
      </div>
    </div>
  </div>
</div>
<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-form">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 contact-option">
                            <div class="contact-option_rsp">
                                <h3>Kalkulator Zakat</h3>
                                <?php echo $this->session->flashdata('msg');?>
                                <form action="<?php echo site_url('home/bayar_zakat');?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Penghasilan/Gaji Saya per Bulan</label>
                                        <input type="number" class="form-control" placeholder="Gaji" name="gaji" required id="ph_gaji" min="0" value="0">
                                    </div>
                                    <!-- // end .form-group -->
                                    <div class="form-group">
                                        <label>Penghasilan Lain-lain Saya per Bulan</label>
                                        <input type="number" class="form-control" placeholder="Penghasilan Lain- lain" name="lain" required id="ph_lain" min="0" value="0">
                                    </div>
                                    <!-- // end .form-group -->
                                    <div class="form-group">
                                        <label>Hutang/Cicilan Saya untuk Kebutuhan Pokok 1)</label>
                                        <input type="number" class="form-control" placeholder="Hutang/ Cicilan" name="hutang" required id="ph_hutang" min="0" value="0">
                                    </div>
                                    <!-- // end .form-group -->
                                    <div class="form-group">
                                        <label>Jumlah Penghasilan per Bulan</label>
                                        <input type="number" class="form-control" placeholder="Jumlah Penghasilan" name="jumlah" required readonly="" id="ph_jumlah" min="0" value="0">
                                    </div>
                                    <div class="form-group">
                                        <label>Harga Emas saat ini</label>
                                        <input type="number" class="form-control" placeholder="Harga Emas" name="harga_emas" required id="ph_harga_emas" min="0" value="<?php echo @$harga_emas; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Besarnya Nisab Zakat Penghasilan per Bulan</label>
                                        <input type="number" class="form-control" placeholder="Nisab" name="nisab" required readonly="" id="ph_nisab" min="0" value="0">
                                    </div>
                                    <div class="form-group">
                                        <label>Apakah Saya Wajib Membayar Zakat Penghasilan?</label><br>
                                        <span class="badge badge-success" style="display:none;" id="zakat-ya">Wajib</span>
                                        <span class="badge badge-light" style="display:none;" id="zakat-tidak">Tidak</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Jumlah yang Saya Harus Bayarkan per Bulan</label>
                                        <input type="number" class="form-control" placeholder="Zakat" name="zakat" required id="ph_zakat" min="0" value="0">
                                    </div>
                                    <fieldset>
                                        <legend style="font-size:14px;line-height:18px;font-style:italic;">Harap isi nama dan upload bukti pembayaran, saat melakukan pembayaran zakat:</legend>
                                        <div class="form-group">
                                            <label>Nama </label>
                                            <input type="text" class="form-control" placeholder="Nama" name="nama" required id="ph_nama" value="Hamba Allah">
                                        </div>
                                        <div class="form-group">
                                            <label>Bukti Pembayaran</label>
                                            <input type="file" name="bukti" class="form-control">
                                        </div>
                                    </fieldset>
                                    <!-- // end .form-group -->
                                    <button type="submit" class="btn btn-primary btn-submit" style="background-color : orange;">SUBMIT</button>
                                    <!-- <div><?php //echo $this->session->flashdata('msg');?></div> -->
                                    <!-- // end #js-contact-result -->
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <h3>Manfaat Bayar Zakat untuk Keuangan</h3>
                            <p style="font-size: 35px;"> فَعَنِ اِبْنِ عَبَّاسٍ رَضِيَ اَللَّهُ عَنْهُمَا: ( أَنَّ اَلنَّبِيَّ صلى الله عليه وسلم بَعَثَ مُعَاذًا رضي الله عنه إِلَى اَلْيَمَنِ )  فَذَكَرَ اَلْحَدِيثَ, وَفِيهِ: ( أَنَّ اَللَّهَ قَدِ اِفْتَرَضَ عَلَيْهِمْ صَدَقَةً فِي أَمْوَالِهِمْ, تُؤْخَذُ مِنْ أَغْنِيَائِهِمْ, فَتُرَدُّ فِي فُقَرَائِهِمْ )  مُتَّفَقٌ عَلَيْهِ, وَاللَّفْظُ لِلْبُخَارِيّ ِ</p>
                            <p>Dari Ibnu Abbas r. bahwa Nabi Shallallaahu 'alaihi wa Sallam mengutus Mu'adz ke negeri Yaman --ia meneruskan hadits itu-- dan didalamnya (beliau bersabda): "Sesungguhnya Allah telah mewajibkan mereka zakat dari harta mereka yang diambil dari orang-orang kaya di antara mereka dan dibagikan kepada orang-orang fakir di antara mereka." Muttafaq Alaihi dan lafadznya menurut Bukhari.</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<script type="text/javascript">
    $(function(){
        var ph_gaji, ph_lain, ph_hutang, ph_harga_emas, ph_nisab;
        var ph_perhitungan, ph_nilai_zakat;

        function hitungGaji(){
            // ambil value pada input number
            ph_gaji = $('#ph_gaji').val();
            ph_lain = $('#ph_lain').val();
            ph_hutang = $('#ph_hutang').val();
            ph_harga_emas = $('#ph_harga_emas').val();
            // jika tdk terdefinisi/ inputan kosong, ganti dgn angka 0
            if(isNaN(ph_gaji)) {
                ph_gaji = 0;
            }
            if(isNaN(ph_lain)) {
                ph_lain = 0;
            }
            if(isNaN(ph_hutang)) {
                ph_hutang = 0;
            }
            if(isNaN(ph_harga_emas)) {
                ph_harga_emas = 0;
            }
            // ubah format dari "string" ke "number"
            // agar bisa dihitung dgn benar
            ph_gaji = parseInt(ph_gaji);
            ph_lain = parseInt(ph_lain);
            ph_hutang = parseInt(ph_hutang);
            ph_harga_emas = parseInt(ph_harga_emas);
            // nisab dlm setahun = harga 85 gram emas
            ph_nisab = ph_harga_emas*85;
            // nisab dlm sebulan = nisab dlm setahun/ 12
            if (ph_nisab > 0) {
                ph_nisab = ph_nisab/12;
            }
            ph_nisab = parseInt(ph_nisab);
            // set nisab per bulan
            $('#ph_nisab').val(ph_nisab);

            // hitung pemasukan bersih
            ph_perhitungan = ph_gaji + ph_lain - ph_hutang;
            $('#ph_jumlah').val(ph_perhitungan);
            // hitung besar zakat
            ph_perhitungan = parseInt(ph_perhitungan);
            ph_nilai_zakat = 0;
            $('#zakat-ya').hide();
            $('#zakat-tidak').show();
            if (ph_perhitungan > ph_nisab) {
                // zakat = 2.5% dari penghasilan
                ph_nilai_zakat = 0.025 * ph_perhitungan;
                $('#zakat-ya').show();
                $('#zakat-tidak').hide();
            }
            $('#ph_zakat').val(ph_nilai_zakat);
        }
        // on keyup
        // ketika nilai inputan berubah (key up), jalankan function ini
        $('#ph_gaji').keyup(function(){
            hitungGaji();
        });
        $('#ph_lain').keyup(function(){
            hitungGaji();
        });
        $('#ph_hutang').keyup(function(){
            hitungGaji();
        });
        $('#ph_harga_emas').keyup(function(){
            hitungGaji();
        });
    });
</script>