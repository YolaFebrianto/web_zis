<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Web Zis</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon_zis.png'?>">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/animate.css">
    
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/ionicons.min.css">
    

    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>minishop/css/style.css">
  </head>
  <?php
        $dataSettinganWeb = get_web_setting();
    ?>
  <body class="goto-here">
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
          <a class="navbar-brand" href="<?php echo  base_url() ?>"><?php echo @$dataSettinganWeb['nama']; ?></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
          </button>

          <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active"><a href="<?php echo base_url() ?>" class="nav-link">Home</a></li>
              <!-- <li class="nav-item"><a href="<?php //echo base_url('login/profil') ?>" class="nav-link">Profil</a></li> -->
              <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tentang Kami</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
                <a class="dropdown-item" href="<?php echo base_url('home/profil') ?>">Profil Lembaga</a>
                <a class="dropdown-item" href="<?php echo base_url('home/visi_misi') ?>">Visi Dan Misi</a>
              </div>
            </li>
            <li class="nav-item"><a href="<?php echo base_url('home/program') ?>" class="nav-link">Program</a></li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Layanan</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
                <!-- <a class="dropdown-item" href="shop.html">Ambulance</a>
                <a class="dropdown-item" href="product-single.html">Jemput Donasi</a>
                <a class="dropdown-item" href="cart.html">Konsultasi online</a> -->
                <a class="dropdown-item" href="<?php echo base_url('home/kalkulator') ?>">Kalkulator Zakat</a>
              </div>
            </li>
              
              <li class="nav-item"><a href="<?php echo base_url('berita') ?>" class="nav-link">Berita</a></li>
              <!-- <li class="nav-item"><a href="<?php //echo base_url('login/logout') ?>" class="nav-link">Logout</a></li> -->
            </ul>
          </div>
        </div>
      </nav>
    <!-- END nav -->
