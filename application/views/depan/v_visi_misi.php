<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url() ?>minishop/images/bg_6.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Tentang Kami</span></p>
        <h1 class="mb-0 bread">Visi Dan Misi</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section">
    <div class="container">
        <div class="row">              
            <div class="col-lg-12 product-details pl-md-5 ftco-animate">
                <?php echo @$isi['halaman_isi']; ?>
            </div>
        </div>
    </div>
</section>