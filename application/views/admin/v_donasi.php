<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Donasi
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Donasi</a></li>
      <li class="active">Data Donasi</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">

        <div class="box">
          <div class="box-header">
            <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Add Donasi</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-striped" style="font-size:13px;">
              <thead>
              <tr>
				<th>Photo</th>
                  <th>Nama Donasi</th>
                  <th>Jumlah Donasi</th>
                  <th style="text-align:center;">Aksi</th>
              </tr>
              </thead>
              <tbody>
			<?php foreach ($data->result_array() as $i) :
                     $id=$i['donasi_id'];
                     $nama=$i['nama_donasi'];
                     // $jumlah=$i['jumlah_donasi'];
                     $gambar=$i['gambar'];
                     $jumlah = get_sum_bayar($id);
              if($id>1):
                  ?>
                  
              <tr>
                <td><img width="40" height="40" class="img-circle" src="<?php echo base_url().'assets/images/'.$gambar;?>"></td>
                <td><?php echo $nama;?></td>
                <td><?php echo number_format($jumlah,2,',','.') ; ?></td>
                <!-- <td><?php //echo $gambar;?></td> -->
                <td style="text-align:right;">
                      <a class="btn" data-toggle="modal" data-target="#modalEdit<?php echo $id;?>"><span class="fa fa-pencil"></span></a>
                      <a class="btn" data-toggle="modal" data-target="#modalHapus<?php echo $id;?>"><span class="fa fa-trash"></span></a>
                </td>
              </tr>
			<?php 
              endif;
            endforeach;?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--Modal Add Pengguna-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Add Donasi</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/donasi/simpan_donasi';?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Judul Donasi</label>
                                <div class="col-sm-7">
                                    <input type="text" name="xjudul" class="form-control" id="inputUserName" placeholder="Masukkan Judul Donasi" required>
                                </div>
                            </div>
                            <!-- <div class="form-group"> -->
                                <!-- <label for="inputEmail3" class="col-sm-4 control-label">Jumlah Donasi</label> -->
                                <!-- <div class="col-sm-7"> -->
                                    <input type="hidden" name="xjumlah" class="form-control" id="inputEmail3" min="0" value="0">
                                <!-- </div> -->
                            <!-- </div> -->
                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                                <div class="col-sm-7">
                                    <input type="file" name="filefoto" required/>
                                </div>
                            </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>


<?php foreach ($data->result_array() as $i) :
             $id_donasi=$i['donasi_id'];
             $judul_donasi=$i['nama_donasi'];
             // $jumlah_donasi=$i['jumlah_donasi'];
             $gambar_donasi=$i['gambar'];
             $jumlah_donasi = get_sum_bayar($id_donasi);
            ?>
<!--Modal Edit Pengguna-->
<div class="modal fade" id="modalEdit<?php echo $id_donasi;?>" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="modalEditLabel">Edit Donasi</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/donasi/update_donasi';?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Judul donasi</label>
                                <div class="col-sm-7">
							<input type="hidden" name="donasi_id" value="<?php echo $id_donasi;?>"/>
                                    <input type="text" name="xjudul" class="form-control" id="inputUserName" value="<?php echo $judul_donasi;?>" placeholder="Nama Lengkap" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Jumlah Donasi</label>
                                <div class="col-sm-7">
                                    <input type="number" name="xjumlah" class="form-control" value="<?php echo $jumlah_donasi;?>" id="inputEmail3" placeholder="Jumlah" min="0" readonly="true">
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                                <div class="col-sm-7">
                                    <input type="file" name="filefoto"/>
                                </div>
                            </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<?php foreach ($data->result_array() as $i) :
             $id_donasi=$i['donasi_id'];
             $judul_donasi=$i['nama_donasi'];
             $jumlah_donasi=$i['jumlah_donasi'];
             $gambar_donasi=$i['gambar'];
            ?>
<!--Modal Hapus Pengguna-->
<div class="modal fade" id="modalHapus<?php echo $id_donasi;?>" tabindex="-1" role="dialog" aria-labelledby="modalHapusLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="modalHapusLabel">Hapus Donasi</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/donasi/hapus_donasi';?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <input type="hidden" name="donasi_id" value="<?php echo $id_donasi;?>"/>
              <input type="hidden" value="<?php echo $gambar_donasi;?>" name="gambar">
                    <p>Apakah Anda yakin mau menghapus Donasi <b><?php echo $judul_donasi;?></b> ?</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>
<?php
  get_admin_js();
?>