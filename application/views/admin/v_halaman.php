<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      List Halaman
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Halaman</a></li>
      <li class="active">List Halaman</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">

        <div class="box">
          <div class="box-header">
            <a class="btn btn-success btn-flat" href="<?php echo base_url().'admin/halaman/add_halaman'?>"><span class="fa fa-plus"></span> Tambah Halaman</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-striped" style="font-size:13px;">
              <thead>
              <tr>
                  <th>No. </th>
        					<th>Judul</th>
                  <th>Slug</th>
                  <th style="text-align:right;">Aksi</th>
              </tr>
              </thead>
              <tbody>
        			<?php
      					$no=0;
      					foreach ($data->result_array() as $k=>$i) :
      					   $no++;
      					   $halaman_id=$i['halaman_id'];
      					   $halaman_judul=$i['halaman_judul'];
      					   $halaman_isi=$i['halaman_isi'];
                   $halaman_slug=$i['halaman_slug'];
              ?>
              <tr>
                <td><?php echo $k+1; ?></td>
                <td><?php echo $halaman_judul;?></td>

      				  <td><?php echo $halaman_slug;?></td>
                <td style="text-align:right;">
                      <a class="btn" href="<?php echo base_url().'admin/halaman/get_edit/'.$halaman_id;?>"><span class="fa fa-pencil"></span></a>
                      <a class="btn" data-toggle="modal" data-target="#modalHapus<?php echo $halaman_id;?>"><span class="fa fa-trash"></span></a>
                </td>
              </tr>
              <?php 
                endforeach;
              ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<?php foreach ($data->result_array() as $i) :
    $halaman_id=$i['halaman_id'];
    $halaman_judul=$i['halaman_judul'];
    $halaman_gambar=$i['halaman_gambar'];
?>
<!--Modal Hapus Pengguna-->
<div class="modal fade" id="modalHapus<?php echo $halaman_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Hapus Halaman</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/halaman/hapus_halaman'?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
			       <input type="hidden" name="kode" value="<?php echo $halaman_id;?>"/>
                <p>Apakah Anda yakin mau menghapus Posting <b><?php echo $halaman_judul;?></b> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>
<?php
  get_admin_js();
?>