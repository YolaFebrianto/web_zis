<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
            <?php
                // $query=$this->db->query("SELECT SUM(a.jumlah) AS saldo FROM (
                //   SELECT SUM(z.jumlah_zakat) AS jumlah FROM tbl_zakat z
                //   UNION 
                //   SELECT SUM(i.jumlah_infaq) AS jumlah FROM tbl_infaq i
                //   UNION 
                //   SELECT SUM(s.jumlah_donasi) AS jumlah FROM tbl_donasi s
                // ) AS a");
                // $jml=$query->row();
                //var_dump($jml->saldo);
                $saldo = get_sum_bayar();
                if(!empty(@$saldo) AND $saldo>0){
                    $saldo = number_format($saldo,2,',','.');
                }
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Saldo</span>
            <span class="info-box-number">Rp. <?php echo @$saldo;?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-newspaper-o"></i></span>
            <?php
                  $query=$this->db->query("SELECT * FROM tbl_tulisan");
                  $jml=$query->num_rows();
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Berita</span>
            <span class="info-box-number"><?php echo $jml;?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-comments"></i></span>
          <?php
                $query=$this->db->query("SELECT * FROM tbl_komentar");
                $jml=$query->num_rows();
          ?>
          <div class="info-box-content">
            <span class="info-box-text">Komentar</span>
            <span class="info-box-number"><?php echo $jml;?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
          <?php
                  $query=$this->db->query("SELECT * FROM tbl_pengguna");
                  $jml=$query->num_rows();
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Pengguna</span>
            <span class="info-box-number"><?php echo $jml;?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Pengunjung bulan ini</h3>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                <div class="col-md-12">
                        <canvas id="canvas" width="1000" height="280"></canvas>
                </div>
                <!-- /.chart-responsive -->
              </div>
              <!-- /.col -->

              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->

          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Posting Populer</h3>

            <table class="table">
            <?php
                $query=$this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC");
                foreach ($query->result_array() as $i) :
                    $tulisan_id=$i['tulisan_id'];
                    $tulisan_judul=$i['tulisan_judul'];
                    $tulisan_views=$i['tulisan_views'];
            ?>
                <tr>
                  <td><?php echo $tulisan_judul;?></td>
                  <td><?php echo $tulisan_views.' Views';?></td>
                </tr>
            <?php endforeach;?>
            </table>
          </div>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4">
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-yellow">
          <span class="info-box-icon"><i class="fa fa-safari"></i></span>
          <?php
                  $query=$this->db->query("SELECT * FROM tbl_pengunjung WHERE pengunjung_perangkat='Safari'");
                  $jml=$query->num_rows();
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Safari</span>
            <span class="info-box-number"><?php echo number_format($jml);?></span>

            <div class="progress">
              <div class="progress-bar" style="width: 100%"></div>
            </div>
                <span class="progress-description">
                  Penggunjung
                </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="fa fa-globe"></i></span>
          <?php
                  $query=$this->db->query("SELECT * FROM tbl_pengunjung WHERE pengunjung_perangkat='Other' OR pengunjung_perangkat='Internet Explorer'");
                  $jml=$query->num_rows();
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Lainnya</span>
            <span class="info-box-number"><?php echo number_format($jml);?></span>

            <div class="progress">
              <div class="progress-bar" style="width: 100%"></div>
            </div>
                <span class="progress-description">
                  Pengunjung
                </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="fa fa-users"></i></span>
          <?php
                  $query=$this->db->query("SELECT * FROM tbl_pengunjung WHERE DATE_FORMAT(pengunjung_tanggal,'%m%y')=DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH),'%m%y')");
                  $jml=$query->num_rows();
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Pengunjung Bulan Lalu</span>
            <span class="info-box-number"><?php echo number_format($jml);?></span>

            <div class="progress">
              <div class="progress-bar" style="width: 100%"></div>
            </div>
                <span class="progress-description">
                  Pengunjung
                </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-aqua">
          <span class="info-box-icon"><i class="fa fa-users"></i></span>
           <?php
                  $query=$this->db->query("SELECT * FROM tbl_pengunjung WHERE DATE_FORMAT(pengunjung_tanggal,'%m%y')=DATE_FORMAT(CURDATE(),'%m%y')");
                  $jml=$query->num_rows();
            ?>
          <div class="info-box-content">
            <span class="info-box-text">Pengunjung Bulan Ini</span>
            <span class="info-box-number"><?php echo number_format($jml);?></span>

            <div class="progress">
              <div class="progress-bar" style="width: 100%"></div>
            </div>
                <span class="progress-description">
                  Pengunjung
                </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->

        <!-- PRODUCT LIST -->

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->