<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Halaman
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Halaman</a></li>
      <li class="active">Update Halaman</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Update Halaman</h3>
      </div>
	<?php
      $b=$data->row_array();
  ?>
	<form action="<?php echo base_url().'admin/halaman/update_halaman'?>" method="post" enctype="multipart/form-data">

      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-10">
            <input type="hidden" name="kode" value="<?php echo $b['halaman_id'];?>">
            <input type="text" name="xjudul" class="form-control" value="<?php echo $b['halaman_judul'];?>" placeholder="Judul berita atau artikel" required/>
          </div>
          <!-- /.col -->
          <div class="col-md-2">
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-flat pull-right"><span class="fa fa-pencil"></span> Update</button>
            <!-- /.form-group -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->

    </div>
  </div>
    <!-- /.box -->

    <div class="row">
      <div class="col-md-12">

        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Halaman</h3>
          </div>
          <div class="box-body">

		       <textarea id="ckeditor" name="xisi" required><?php echo $b['halaman_isi'];?></textarea>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col (right) -->
    </div>
	</form>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.

    CKEDITOR.replace('ckeditor');


  });
</script>