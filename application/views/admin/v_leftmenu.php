<!--Counter Inbox-->
<?php
    $query=$this->db->query("SELECT * FROM tbl_inbox WHERE inbox_status='1'");
    $query2=$this->db->query("SELECT * FROM tbl_komentar WHERE komentar_status='0'");
    $setting=$this->db->query("SELECT * FROM tbl_setting WHERE id='1'")->row_array();
    $jum_comment=$query2->num_rows();
    $jum_pesan=$query->num_rows();
?>
<header class="main-header">

    <!-- Logo -->
    <a href="<?php echo base_url().'admin/dashboard'?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?php echo @$setting['nama']; ?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?php echo @$setting['nama']; ?></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success"><?php echo $jum_pesan;?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Anda memiliki <?php echo $jum_pesan;?> pesan</li>
              <li>
                <ul class="menu">
                <?php
                    $inbox=$this->db->query("SELECT tbl_inbox.*,DATE_FORMAT(inbox_tanggal,'%d %M %Y') AS tanggal FROM tbl_inbox WHERE inbox_status='1' ORDER BY inbox_id DESC LIMIT 5");
                    foreach ($inbox->result_array() as $in) :
                        $inbox_id=$in['inbox_id'];
                        $inbox_nama=$in['inbox_nama'];
                        $inbox_tgl=$in['tanggal'];
                        $inbox_pesan=$in['inbox_pesan'];
                ?>
                  <li>
                    <a href="<?php echo base_url().'admin/inbox'?>">
                      <div class="pull-left">
                        <img src="<?php echo base_url().'assets/images/user_blank.png'?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        <?php echo $inbox_nama;?>
                        <small><i class="fa fa-clock-o"></i> <?php echo $inbox_tgl;?></small>
                      </h4>
                      <p><?php echo $inbox_pesan;?></p>
                    </a>
                  </li>
                  <?php endforeach;?>
                </ul>
              </li>
              <li class="footer"><a href="<?php echo base_url().'admin/inbox'?>">Lihat Semua Pesan</a></li>
            </ul>
          </li> -->

          <?php
              $id_admin=$this->session->userdata('idadmin');
              $q=$this->db->query("SELECT * FROM tbl_pengguna WHERE pengguna_id='$id_admin'");
              $c=$q->row_array();
          ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url().'assets/images/'.$c['pengguna_photo'];?>" class="user-image" alt="">
              <span class="hidden-xs"><?php echo $c['pengguna_nama'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url().'assets/images/'.$c['pengguna_photo'];?>" class="img-circle" alt="">

                <p>
                  <?php echo $c['pengguna_nama'];?>
                  <?php if($c['pengguna_level']=='1'):?>
                    <small>Administrator</small>
                  <?php else:?>
                    <small>Author</small>
                  <?php endif;?>
                </p>
              </li>
              <!-- Menu Body -->

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat" data-toggle="modal" data-target="#ModalEdit">Ubah Profil</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url().'admin/login/logout'?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="<?php echo base_url().''?>" target="_blank" title="Lihat Website"><i class="fa fa-globe"></i></a>
          </li>
        </ul>
      </div>

    </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <?php
    $uri2 = $this->uri->segment(2);
  ?>
  <ul class="sidebar-menu">
    <li class="header">Menu Utama</li>
    <li class="<?php echo ($uri2=='dashboard')?'active':''; ?>">
      <a href="<?php echo base_url().'admin/dashboard'?>">
        <i class="fa fa-home"></i> <span>Dashboard</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li  class="<?php echo ($uri2=='pengguna')?'active':''; ?>">
      <a href="<?php echo base_url().'admin/pengguna'?>">
        <i class="fa fa-users"></i> <span>Pengguna</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li class="treeview <?php echo ($uri2=='tulisan' OR $uri2=='kategori')?'active':''; ?>">
      <a href="#">
        <i class="fa fa-newspaper-o"></i>
        <span>Berita</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="<?php echo ($uri2=='tulisan')?'active':''; ?>"><a href="<?php echo base_url().'admin/tulisan'?>"><i class="fa fa-list"></i> List Berita</a></li>
        <li class="<?php echo ($uri2=='kategori')?'active':''; ?>"><a href="<?php echo base_url().'admin/kategori'?>"><i class="fa fa-wrench"></i> Kategori</a></li>
      </ul>
    </li>
    <li class="<?php echo ($uri2=='halaman')?'active':''; ?>">
      <a href="<?php echo base_url().'admin/halaman'?>">
        <i class="fa fa-file"></i> <span>Halaman</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li class="<?php echo ($uri2=='komentar')?'active':''; ?>">
      <a href="<?php echo base_url().'admin/komentar'?>">
        <i class="fa fa-comments"></i> <span>Komentar</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-green"><?php echo $jum_comment;?></small>
        </span>
      </a>
    </li>
<!--     <li>
      <a href="<?php echo base_url().'admin/agenda'?>">
        <i class="fa fa-calendar"></i> <span>Agenda</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li>
      <a href="<?php echo base_url().'admin/pengumuman'?>">
        <i class="fa fa-volume-up"></i> <span>Pengumuman</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li>
      <a href="<?php echo base_url().'admin/files'?>">
        <i class="fa fa-download"></i> <span>Download</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-camera"></i>
        <span>Gallery</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url().'admin/album'?>"><i class="fa fa-clone"></i> Album</a></li>
        <li><a href="<?php echo base_url().'admin/galeri'?>"><i class="fa fa-picture-o"></i> Photos</a></li>
      </ul>
    </li>

    <li>
      <a href="<?php echo base_url().'admin/guru'?>">
        <i class="fa fa-graduation-cap"></i> <span>Data Guru</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="fa fa-user"></i>
        <span>Kesiswaan</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url().'admin/siswa'?>"><i class="fa fa-users"></i> Data Siswa</a></li>
        <li><a href="#"><i class="fa fa-star-o"></i> Prestasi Siswa</a></li>

      </ul>
    </li>

    <li>
      <a href="<?php echo base_url().'admin/inbox'?>">
        <i class="fa fa-envelope"></i> <span>Inbox</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-green"><?php echo $jum_pesan;?></small>
        </span>
      </a>
    </li> -->
    <li class="treeview <?php echo ($uri2=='zakat' OR $uri2=='donasi' OR $uri2=='infaq')?'active':''; ?>">
      <a href="#">
        <i class="fa fa-newspaper-o"></i>
        <span>Program</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="<?php echo ($uri2=='zakat')?'active':''; ?>"><a href="<?php echo base_url().'admin/zakat'?>"><i class="fa fa-money"></i> Zakat</a></li>
        <li class="<?php echo ($uri2=='donasi')?'active':''; ?>"><a href="<?php echo base_url().'admin/donasi'?>"><i class="fa fa-money"></i> Donasi</a></li>
        <li class="<?php echo ($uri2=='infaq')?'active':''; ?>"><a href="<?php echo base_url().'admin/infaq'?>"><i class="fa fa-money"></i> Infaq</a></li>
      </ul>
    </li>
    <!-- <li>
      <a href="<?php //echo base_url().'admin/zakat'?>">
        <i class="fa fa-money"></i> <span>Data Zakat</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
     <li>
      <a href="<?php //echo base_url().'admin/donasi'?>">
        <i class="fa fa-money"></i> <span>Data Donasi</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li>
      <a href="<?php //echo base_url().'admin/infaq'?>">
        <i class="fa fa-money"></i> <span>Data infaq</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li> -->
    <li class="<?php echo ($uri2=='bayar')?'active':''; ?>">
      <a href="<?php echo base_url().'admin/bayar'?>">
        <i class="fa fa-money"></i> <span>Pembayaran</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
    <li class="<?php echo ($uri2=='setting')?'active':''; ?>">
      <a href="<?php echo base_url().'admin/setting'?>">
        <i class="fa fa-cogs"></i> <span>Setting</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>
     <li>
      <a href="<?php echo base_url().'administrator/logout'?>">
        <i class="fa fa-sign-out"></i> <span>Sign Out</span>
        <span class="pull-right-container">
          <small class="label pull-right"></small>
        </span>
      </a>
    </li>


  </ul>
</section>
<!-- /.sidebar -->
</aside>

<!-- MODAL UBAH PROFIL -->
<?php
  $pengguna_id=$c['pengguna_id'];
  $pengguna_nama=$c['pengguna_nama'];
  $pengguna_jenkel=$c['pengguna_jenkel'];
  $pengguna_email=$c['pengguna_email'];
  $pengguna_username=$c['pengguna_username'];
  $pengguna_password=$c['pengguna_password'];
  $pengguna_nohp=$c['pengguna_nohp'];
  $pengguna_level=$c['pengguna_level'];
  $pengguna_photo=$c['pengguna_photo'];
?>
<!--Modal Edit Pengguna-->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Profil</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/pengguna/update_pengguna'?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-7">
                      <input type="hidden" name="kode" value="<?php echo $pengguna_id;?>"/>
                      <input type="text" name="xnama" class="form-control" id="inputUserName" value="<?php echo $pengguna_nama;?>" placeholder="Nama Lengkap" required>
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                  <div class="col-sm-7">
                      <input type="email" name="xemail" class="form-control" value="<?php echo $pengguna_email;?>" id="inputEmail3" placeholder="Email" required>
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Jenis Kelamin</label>
                  <div class="col-sm-7">
                    <?php if($pengguna_jenkel=='L'):?>
                      <div class="radio radio-info radio-inline">
                          <input type="radio" id="inlineRadio1" value="L" name="xjenkel" checked>
                          <label for="inlineRadio1"> Laki-Laki </label>
                      </div>
                      <div class="radio radio-info radio-inline">
                          <input type="radio" id="inlineRadio1" value="P" name="xjenkel">
                          <label for="inlineRadio2"> Perempuan </label>
                      </div>
                    <?php else:?>
                      <div class="radio radio-info radio-inline">
                          <input type="radio" id="inlineRadio1" value="L" name="xjenkel">
                          <label for="inlineRadio1"> Laki-Laki </label>
                      </div>
                      <div class="radio radio-info radio-inline">
                          <input type="radio" id="inlineRadio1" value="P" name="xjenkel" checked>
                          <label for="inlineRadio2"> Perempuan </label>
                      </div>
                    <?php endif;?>
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Username</label>
                  <div class="col-sm-7">
                      <input type="text" name="xusername" class="form-control" value="<?php echo $pengguna_username;?>" id="inputUserName" placeholder="Username" required>
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
                  <div class="col-sm-7">
                      <input type="password" name="xpassword" class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputPassword4" class="col-sm-4 control-label">Ulangi Password</label>
                  <div class="col-sm-7">
                      <input type="password" name="xpassword2" class="form-control" id="inputPassword4" placeholder="Ulangi Password">
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Kontak Person</label>
                  <div class="col-sm-7">
                      <input type="text" name="xkontak" class="form-control" value="<?php echo $pengguna_nohp;?>" id="inputUserName" placeholder="Kontak Person" required>
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Level</label>
                  <div class="col-sm-7">
                      <select class="form-control" name="xlevel" required>
                          <?php if($pengguna_level=='1'):?>
                          <option value="1" selected>Administrator</option>
                          <option value="2">Author</option>
                          <?php else:?>
                          <option value="1">Administrator</option>
                          <option value="2" selected>Author</option>
                          <?php endif;?>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                  <div class="col-sm-7">
                      <input type="file" name="filefoto"/>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>