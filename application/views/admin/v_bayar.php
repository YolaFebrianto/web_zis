<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Pembayaran
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Pembayaran</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <!-- <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> Add Pembayaran</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <?php echo $this->session->flashdata('msg');?>
            <table id="example1" class="table table-striped" style="font-size:13px;">
              <thead>
              <tr>
        				<th>No.</th>
        				<th>Nama</th>
        				<th>Tanggal</th>
        				<th>Nilai</th>
                <th>Jenis Program</th>
                <th>Nama Program</th>
        				<th>Bukti</th>
                <th>Status</th>
                <th style="text-align:right;">Aksi</th>
              </tr>
              </thead>
              <tbody>
				<?php
					$no=0;
					foreach ($data->result_array() as $i) :
					   $no++;
					   $id=$i['bayar_id'];
					   $nama=$i['bayar_nama'];
					   $tanggal=$i['bayar_tanggal'];
					   $nilai=$i['bayar_nilai'];
					   $bukti=$i['bayar_bukti'];
					   $status=$i['bayar_status'];
             $jenis_program=$i['bayar_jenis_program'];             
             $id_program=$i['bayar_id_program'];
             if ($jenis_program=='ZAKAT') {
                $q=$this->db->query("SELECT * FROM tbl_zakat WHERE zakat_id='$id_program'");
                $c=$q->row_array();
                $jenis_aksi='aksizakat';
                $program = @$c['nama_zakat'];
             } else if ($jenis_program=='INFAQ') {
                $q=$this->db->query("SELECT * FROM tbl_infaq WHERE infaq_id='$id_program'");
                $c=$q->row_array();
                $jenis_aksi='aksiinfaq';
                $program = @$c['nama_infaq'];
             } else if ($jenis_program=='DONASI') {
                $q=$this->db->query("SELECT * FROM tbl_donasi WHERE donasi_id='$id_program'");
                $c=$q->row_array();
                $jenis_aksi='aksidonasi';
                $program = @$c['nama_donasi'];
             } else {
                $program = '';
             }

                       if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
                           $tanggal = date('d/m/Y',strtotime($tanggal));
                       }
                  ?>
              <tr>
                <td><?php echo $no;?></td>
                <td><?php echo @$nama;?></td>
      			    <td><?php echo @$tanggal;?></td>
                <td><?php echo number_format($nilai,0,',','.');?></td>
                <td><?php echo @$jenis_program;?></td>
                <td>
                  <?php
                    if (!empty($program)) {
                  ?>
                  <a href="<?php echo site_url('aksi/'.$jenis_aksi.'/'.$id_program);?>" target="_blank"><?php echo $program;?></a>
                  <?php
                    } else {
                      echo @$program;
                    }
                  ?>
                </td>
                <td>
                <?php if(empty($bukti)):?>
                  <a href="<?php echo base_url().'assets/images/user_blank.png';?>" target="_blank">
                    <img width="40" height="40" class="img-circle" src="<?php echo base_url().'assets/images/user_blank.png';?>">
                  </a>
                <?php else:?>
                  <a href="<?php echo base_url().'assets/bukti/'.$bukti;?>" target="_blank">
                    <img width="40" height="40" class="img-circle" src="<?php echo base_url().'assets/bukti/'.$bukti;?>">
                  </a>
                <?php endif;?>
                </td>
                <?php if($status==1):?>
                <td>Sudah Divalidasi</td>
                <?php else:?>
                <td>Belum Divalidasi</td>
                <?php endif;?>
                <td style="text-align:right;">
                      <!-- <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php //echo $id;?>"><span class="fa fa-pencil"></span></a> -->
                      <?php if($status!=1):?>
                      <a class="btn" href="<?php echo site_url('admin/bayar/publish/'.$id);?>" title="Publish"><span class="fa fa-send"></span></a>
                      <?php endif;?>
                      <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $id;?>"><span class="fa fa-trash"></span></a>
                </td>
              </tr>
			<?php endforeach;?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
    // $q2=$this->db->query("SELECT * FROM tbl_pengguna WHERE pengguna_status='1'");
    // $userAll=$q2->result();
?>
<!--Modal Add -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Add Pembayaran</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/bayar/simpan'?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="form-group">
                    <label for="inputNama" class="col-sm-4 control-label">Nama</label>
                    <div class="col-sm-7">
                        <input type="text" name="bayar_nama" class="form-control" id="inputNama" placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputTanggal" class="col-sm-4 control-label">Tanggal</label>
                    <div class="col-sm-7">
                        <!-- <input type="text" name="bayar_tanggal" class="form-control" id="inputTanggal" placeholder="Contoh: 25 September 1993" required> -->
                        <div class="input-group date" data-date="" data-date-format="yyyy-mm-dd">
                            <input class="form-control input-sm input-tanggal" type="text" name="bayar_tanggal" placeholder="Tanggal Kirim" required readonly />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputNilai" class="col-sm-4 control-label">Nilai</label>
                    <div class="col-sm-7">
                        <input type="text" name="bayar_nilai" class="form-control" id="inputNilai" placeholder="Nilai" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputJenisProgram" class="col-sm-4 control-label">Jenis Program</label>
                    <div class="col-sm-7">
                        <select name="bayar_jenis_program" class="form-control">
                            <option value="ZAKAT">Zakat</option>
                            <option value="INFAQ">Infaq</option>
                            <option value="DONASI">Donasi</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputIdProgram" class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-7">
                        <select name="bayar_id_program" class="form-control">
                            <option value="1">Zakat</option>
                            <option value="2">Infaq</option>
                            <option value="3">Donasi</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputStatus" class="col-sm-4 control-label">Status</label>
                    <div class="col-sm-7">
                       <div class="radio radio-info radio-inline">
                            <input type="radio" id="inlineRadio1" value="1" name="bayar_status">
                            <label for="inlineRadio1"> Sudah Divalidasi </label>
                        </div>
                        <div class="radio radio-info radio-inline">
                            <input type="radio" id="inlineRadio1" value="0" name="bayar_status" checked>
                            <label for="inlineRadio2"> Belum Divalidasi </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputBukti" class="col-sm-4 control-label">Bukti</label>
                    <div class="col-sm-7">
                        <input type="file" name="filefoto"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!--Modal Edit -->
<?php foreach ($data->result_array() as $i) :
       $id=@$i['bayar_id'];
       $nama=@$i['bayar_nama'];
       $tanggal=@$i['bayar_tanggal'];
       $nilai=@$i['bayar_nilai'];
       $bukti=@$i['bayar_bukti'];
       $status=@$i['bayar_status'];
       $jenis_program=@$i['bayar_jenis_program'];
       $id_program=@$i['bayar_id_program'];
       if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
           $tanggal = date('Y-m-d',strtotime($tanggal));
       }
?>
<div class="modal fade" id="ModalEdit<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Pembayaran</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/bayar/update'?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <input type="hidden" name="bayar_id" value="<?php echo $id;?>"/>
                <input type="hidden" value="<?php echo $bukti;?>" name="bayar_bukti">
                <div class="form-group">
                    <label for="inputNama" class="col-sm-4 control-label">Nama</label>
                    <div class="col-sm-7">
                        <input type="text" name="bayar_nama" class="form-control" id="inputNama" placeholder="Nama" required value="<?php echo $nama; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputTanggal" class="col-sm-4 control-label">Tanggal</label>
                    <div class="col-sm-7">
                        <!-- <input type="text" name="bayar_tanggal" class="form-control" id="inputTanggal" placeholder="Contoh: 25 September 1993" required value="<?php //echo $tanggal; ?>"> -->
                        <div class="input-group date" data-date="<?php echo @$tanggal;?>" data-date-format="yyyy-mm-dd">
                            <input class="form-control input-sm input-tanggal" type="text" name="deadline" placeholder="Tanggal Kirim" required readonly value="<?php echo @$tanggal;?>" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputNilai" class="col-sm-4 control-label">Nilai</label>
                    <div class="col-sm-7">
                        <input type="text" name="bayar_nilai" class="form-control" id="inputNilai" placeholder="Nilai" required value="<?php echo $nilai; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputStatus" class="col-sm-4 control-label">Status</label>
                    <div class="col-sm-7">
                      <?php if($status=='1'):?>
                       <div class="radio radio-info radio-inline">
                            <input type="radio" id="inlineRadio1" value="1" name="bayar_status" checked>
                            <label for="inlineRadio1"> Sudah Divalidasi </label>
                        </div>
                        <div class="radio radio-info radio-inline">
                            <input type="radio" id="inlineRadio1" value="0" name="bayar_status">
                            <label for="inlineRadio2"> Belum Divalidasi </label>
                        </div>
                      <?php else:?>
                        <div class="radio radio-info radio-inline">
                            <input type="radio" id="inlineRadio1" value="1" name="bayar_status">
                            <label for="inlineRadio1"> Sudah Divalidasi </label>
                        </div>
                        <div class="radio radio-info radio-inline">
                            <input type="radio" id="inlineRadio1" value="0" name="bayar_status" checked>
                            <label for="inlineRadio2"> Belum Divalidasi </label>
                        </div>
                      <?php endif;?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputJenisProgram" class="col-sm-4 control-label">Jenis Program</label>
                    <div class="col-sm-7">
                        <select name="bayar_jenis_program" class="form-control">
                            <option value="">-- pilih --</option>
                            <option value="ZAKAT" <?php echo ($jenis_program=='ZAKAT')?'selected':''; ?>>Zakat</option>
                            <option value="INFAQ" <?php echo ($jenis_program=='INFAQ')?'selected':''; ?>>Infaq</option>
                            <option value="DONASI" <?php echo ($jenis_program=='DONASI')?'selected':''; ?>>Donasi</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputIdProgram" class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-7">
                        <select name="bayar_id_program" class="form-control">
                            <option value="1">Zakat</option>
                            <option value="2">Infaq</option>
                            <option value="3">Donasi</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputBukti" class="col-sm-4 control-label">Bukti</label>
                    <div class="col-sm-7">
                        <input type="file" name="filefoto"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>
<!--Modal Edit -->

<?php foreach ($data->result_array() as $i) :
       $id=$i['bayar_id'];
       $nama=$i['bayar_nama'];
       $tanggal=$i['bayar_tanggal'];
       $nilai=$i['bayar_nilai'];
       $bukti=$i['bayar_bukti'];

       if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
           $tanggal = date('d-m-Y',strtotime($tanggal));
       }
?>
<!--Modal Hapus -->
<div class="modal fade" id="ModalHapus<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="modal-title" id="myModalLabel">Hapus Pembayaran</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/bayar/hapus'?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
			       <input type="hidden" name="bayar_id" value="<?php echo $id;?>"/>
             <input type="hidden" value="<?php echo $bukti;?>" name="bayar_bukti">
                <p>Apakah Anda yakin mau menghapus pembayaran atas nama: <b><?php echo @$nama;?></b>, sebesar: <b><?php echo $nilai;?></b> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>
<?php
  get_admin_js();
?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/datepicker/bootstrap-datepicker3.css">
<style type="text/css">
  /*div.datepicker.dropdown-menu{
    left: 25px !important;
  }*/
</style>
<script src="<?=base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$().ready(function() {
  $(".input-group.date").datepicker({
      format: "yyyy-mm-dd",
    autoclose: true, 
    todayHighlight: false
  });

  $(".input-tanggal").on('click', function(){
    if ($(this).hasClass('focused')) {
      $(this).parent().datepicker('hide');
    } else {
      $(this).parent().datepicker('show');
    }
    $(this).toggleClass('focused');
  });
  $("span.input-group-addon").on('click', function(){
    if ($(this).hasClass('focused')) {
      $(this).parent().datepicker('hide');
    } else {
      $(this).parent().datepicker('show');
    }
    $(this).toggleClass('focused');
  });
});
</script>