<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_login');
		$this->load->model('M_setting');
	}
	function index(){
        $this->load->view('depan/v_login');
    }
	function auth(){
		$username=strip_tags(str_replace("'", "", $this->input->post('username')));
		$password=strip_tags(str_replace("'", "", $this->input->post('password')));
		$u=$username;
		$p=$password;
		$cadmin=$this->M_login->cekadmin($u,$p);
		//echo json_encode($cadmin);
		if($cadmin->num_rows() > 0){
			$this->session->set_userdata('masukDepan',true);
			$this->session->set_userdata('userDepan',$u);
			$dataUser = $cadmin->row_array();
			$this->session->set_userdata('userIdDepan',@$dataUser['pengguna_id']);
			$xcadmin=$cadmin->row_array();
			redirect('home');
		}else{
			echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Username Atau Password Salah</div>');
			redirect('login');
		}
	}
	function profil(){
        $q=$this->M_setting->get();
        $setting=$q->row_array();
        $x['harga_emas']=0;
        if (!empty($setting)) {
            if (!empty(@$setting['harga_emas'])) {
                $x['harga_emas']=@$setting['harga_emas'];
            }
        }        
		$dataUser = @$this->session->userdata('userIdDepan');
		$x['profilData']=$this->M_login->profil($dataUser)->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_profil_pengguna',$x);
		$this->load->view('depan/v_footer');
	}
    function logout(){
        $this->session->sess_destroy();
        redirect('home');
    }
}