<?php
class Program extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_tulisan');
		$this->load->model('M_zakat');
		$this->load->model('M_donasi');
		$this->load->model('M_infaq');
		$this->load->model('M_bayar');
		$this->load->library('upload');
		// $this->load->model('M_pengunjung');
  //   	$this->load->model('M_setting');
		// $this->M_pengunjung->count_visitor();
		// if($this->session->userdata('masukDepan') !=TRUE){
  //           $url=base_url('login');
  //           redirect($url);
  //       };
	}
	function aksizakat($id){
		$x['zakat']=$this->M_zakat->get_zakat_by_kode($id)->row_array();
		$x['nilai']=$this->M_bayar->get_sum_bayar_id_program($id)->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_aksi_zakat',$x);
		$this->load->view('depan/v_footer');
	}
	function aksidonasi($id){
		$x['donasi']=$this->M_donasi->get_donasi_by_kode($id)->row_array();
		$x['nilai']=$this->M_bayar->get_sum_bayar_id_program($id)->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_aksi_donasi',$x);
		$this->load->view('depan/v_footer');
	}
	function aksiinfaq($id){
		$x['infaq']=$this->M_infaq->get_infaq_by_kode($id)->row_array();
		$x['nilai']=$this->M_bayar->get_sum_bayar_id_program($id)->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_aksi_infaq',$x);
		$this->load->view('depan/v_footer');
	}

	function bayar()
	{
		if (isset($_POST['bayar_nilai'])) {
			$bayar_nilai = $_POST['bayar_nilai'];
			$bayar_nama = $_POST['bayar_nama'];
			$bayar_jenis_program = $_POST['bayar_jenis_program'];
			$bayar_id_program = $_POST['bayar_id_program'];
			// $bayar_user_id = @$this->session->userdata('userIdDepan');

			if ($bayar_nilai<=0) {
	            $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Nominal Pembayaran '.@$bayar_jenis_program.' Harus > 0</div>');
	            redirect('home/kalkulator');
			}

			$config['upload_path'] = './assets/bukti/'; //path folder
		    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		    $this->upload->initialize($config);
		    if(!empty($_FILES['bayar_bukti']['name']))
		    {
		        if ($this->upload->do_upload('bayar_bukti'))
		        {
	                $gbr = $this->upload->data();
	                //Compress Image
	                $config['image_library']='gd2';
	                $config['source_image']='./assets/bukti/'.$gbr['file_name'];
	                $config['create_thumb']= FALSE;
	                $config['maintain_ratio']= FALSE;
	                $config['quality']= '60%';
	                $config['width']= 300;
	                $config['height']= 300;
	                $config['new_image']= './assets/bukti/'.$gbr['file_name'];
	                $this->load->library('image_lib', $config);
	                $this->image_lib->resize();

	                $bayar_bukti=$gbr['file_name'];

					$this->M_bayar->bayar_program($bayar_nama,$bayar_nilai,$bayar_bukti,$bayar_jenis_program,$bayar_id_program);
					$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pembayaran '.@$bayar_jenis_program.' Berhasil!</div>');
					redirect('home/program');
	            } else {
		            $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Bukti Gagal</div>');
		            redirect('home/program');
	            }
	        } else {
				$this->M_bayar->bayar_program_tanpa_gambar($bayar_nama,$bayar_nilai,$bayar_jenis_program,$bayar_id_program);
				$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pembayaran '.@$bayar_jenis_program.' Berhasil!</div>');
				redirect('home/program');
			}
		}
	}
}
?>