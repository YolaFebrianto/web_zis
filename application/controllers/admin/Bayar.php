<?php
class Bayar extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_bayar');
		$this->load->model('M_bayar');
		$this->load->model('M_infaq');
		$this->load->model('M_donasi');
		$this->load->library('upload');
	}


	function index(){
		$x['data']=$this->M_bayar->get_all_bayar();
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_bayar',$x);
		$this->load->view('admin/v_footer');
	}

    function publish(){
        $kode = htmlspecialchars($this->uri->segment(4),ENT_QUOTES);
        $this->M_bayar->publish($kode);
        echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Berhasil mengubah status</div>');
        redirect('admin/bayar');
    }
	
	function simpan(){
		$config['upload_path'] = './assets/bukti/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name']))
	    {
	        if ($this->upload->do_upload('filefoto'))
	        {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/bukti/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/bukti/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $bukti=$gbr['file_name'];
				$nama=strip_tags($this->input->post('bayar_nama'));
				$tanggal=strip_tags($this->input->post('bayar_tanggal'));
		       	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
		           $tanggal = date('Y-m-d',strtotime($tanggal));
		       	}
				$nilai=strip_tags($this->input->post('bayar_nilai'));
				$status=strip_tags($this->input->post('bayar_status'));
				$jenis_program=strip_tags($this->input->post('bayar_jenis_program'));
				$id_program=strip_tags($this->input->post('bayar_id_program'));

				$this->M_bayar->bayar_program($user_id,$tanggal,$nilai,$status,$bukti,$jenis_program,$id_program);
				echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pembayaran '.@$jenis_program.' Berhasil!</div>');
				redirect('admin/bayar');
			}else{
	            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Bukti Gagal</div>');
	            redirect('admin/bayar');
	        }
	         
	    }else{
			$nama=strip_tags($this->input->post('bayar_nama'));
			$tanggal=strip_tags($this->input->post('bayar_tanggal'));
	       	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
	           $tanggal = date('Y-m-d',strtotime($tanggal));
	       	}
			$nilai=strip_tags($this->input->post('bayar_nilai'));
			$status=strip_tags($this->input->post('bayar_status'));
			$jenis_program=strip_tags($this->input->post('bayar_jenis_program'));
			$id_program=strip_tags($this->input->post('bayar_id_program'));
			$this->M_bayar->bayar_program_tanpa_gambar($user_id,$tanggal,$nilai,$status,$jenis_program,$id_program);
			echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pembayaran '.@$jenis_program.' Berhasil!</div>');
			redirect('admin/bayar');
		}
				
	}
	
	function update(){		
        $config['upload_path'] = './assets/bukti/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/bukti/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/bukti/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$this->input->post('gambar');
				$path='./assets/bukti/'.$gambar;
				unlink($path);

                $bukti=$gbr['file_name'];
                $id=$this->input->post('id');
				$user_id=strip_tags($this->input->post('zakat_user_id'));
				$tanggal=strip_tags($this->input->post('zakat_tanggal'));
		       	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
		           $tanggal = date('Y-m-d',strtotime($tanggal));
		       	}
				$nilai=strip_tags($this->input->post('zakat_nilai'));
				$status=strip_tags($this->input->post('zakat_status'));

				$this->M_bayar->update($id,$user_id,$tanggal,$nilai,$status,$bukti);
				$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Update Pembayaran Berhasil!</div>');
				redirect('admin/bayar');
            }else{
                echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Bukti Gagal</div>');
                redirect('admin/bayar');
            }	                
        }else{
			$id=$this->input->post('id');
			$user_id=strip_tags($this->input->post('zakat_user_id'));
			$tanggal=strip_tags($this->input->post('zakat_tanggal'));
	       	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
	           $tanggal = date('Y-m-d',strtotime($tanggal));
	       	}
			$nilai=strip_tags($this->input->post('zakat_nilai'));
			$status=strip_tags($this->input->post('zakat_status'));
			$this->M_bayar->update_tanpa_img($id,$user_id,$tanggal,$nilai,$status);
			$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Update Pembayaran Berhasil!</div>');
			redirect('admin/bayar');
        } 
	}

	function hapus(){
		$id=$this->input->post('bayar_id');
		$bukti=$this->input->post('bayar_bukti');
		$jenis=$this->input->post('bayar_jenis_program');
		$path='./assets/bukti/'.$bukti;
		unlink($path);
		$this->M_bayar->hapus($id);
		echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Hapus '.@$jenis.' Berhasil!</div>');
		redirect('admin/bayar');
	}
}