<?php
class Halaman extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_halaman');
	}

	function index(){
		$x['data']=$this->M_halaman->get_all_halaman();
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_halaman',$x);
		$this->load->view('admin/v_footer');
	}
	function add_halaman(){
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_add_halaman');
		$this->load->view('admin/v_footer');
	}
	function get_edit(){
		$kode=$this->uri->segment(4);
		$x['data']=$this->M_halaman->get_halaman_by_kode($kode);
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_edit_halaman',$x);
		$this->load->view('admin/v_footer');
	}
	function simpan_halaman(){
		$judul=strip_tags($this->input->post('xjudul'));
		$isi=$this->input->post('xisi');
		$string   = preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $judul);
		$trim     = trim($string);
		$slug     = strtolower(str_replace(" ", "-", $trim));
		try {
			$this->M_halaman->simpan_halaman($judul,$isi,$slug);
			echo $this->session->set_flashdata('msg','data berhasil disimpan');
		} catch (Exception $e) {
			echo $this->session->set_flashdata('msg','data gagal disimpan!');
		}
		redirect('admin/halaman');
	}

	function update_halaman(){
        $halaman_id=$this->input->post('kode');
        $judul=strip_tags($this->input->post('xjudul'));
		$isi=$this->input->post('xisi');
		$string   = preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $judul);
		$trim     = trim($string);
		$slug     = strtolower(str_replace(" ", "-", $trim));
		try {
			$this->M_halaman->update_halaman($halaman_id,$judul,$isi,$slug);
			echo $this->session->set_flashdata('msg','data berhasil diupdate');
		} catch (Exception $e) {
			echo $this->session->set_flashdata('msg','data gagal diupdate!');
			
		}
		redirect('admin/halaman');
	}

	function hapus_halaman(){
		$kode=$this->input->post('kode');
		try {
			$this->M_halaman->hapus_halaman($kode);
			echo $this->session->set_flashdata('msg','data berhasil dihapus');
		} catch (Exception $e) {
			echo $this->session->set_flashdata('msg','data gagal dihapus!');
		}
		redirect('admin/halaman');
	}
}