<?php
class Infaq extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_infaq');
		$this->load->library('upload');
	}


	function index(){
		$id=$this->session->userdata('idadmin');
		// $x['user']=$this->M_infaq->get_pengguna_login($kode);
		$x['data']=$this->M_infaq->get_all_infaq();
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_infaq',$x);
		$this->load->view('admin/v_footer');
	}

	function simpan_infaq(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name']))
	    {
	        if ($this->upload->do_upload('filefoto'))
	        {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
				$nama=strip_tags($this->input->post('xjudul'));
				$jumlah=strip_tags($this->input->post('xjumlah'));

				$this->M_infaq->simpan_infaq($nama,$jumlah,$gambar);
				echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Tambah Infaq Berhasil!</div>');
				redirect('admin/infaq');
			}else{
	            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Gambar Gagal</div>');
	            redirect('admin/infaq');
	        }
	         
	    }else{
			$nama=strip_tags($this->input->post('xjudul'));
			$jumlah=strip_tags($this->input->post('xjumlah'));
			$this->M_infaq->simpan_infaq_tanpa_gambar($nama,$jumlah);
			echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Tambah Infaq Berhasil!</div>');
			redirect('admin/infaq');
		}
				
	}

	function update_infaq(){				
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
                $nama=$this->input->post('xjudul');
                $jumlah=$this->input->post('xjumlah');
                $infaq_id=$this->input->post('infaq_id');
				$this->M_infaq->update_infaq($nama,$jumlah,$gambar,$infaq_id);
				echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Edit Infaq Berhasil!</div>');
				redirect('admin/infaq');
            }else{
	            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Gambar Gagal</div>');
	            redirect('admin/infaq');
	        }
	    }else{
			$nama=strip_tags($this->input->post('xjudul'));
			$jumlah=strip_tags($this->input->post('xjumlah'));
                $infaq_id=$this->input->post('infaq_id');
			$this->M_infaq->update_infaq_tanpa_gambar($nama,$jumlah,$infaq_id);
			echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Edit Infaq Berhasil!</div>');
			redirect('admin/infaq');
		}
	                
	}

	function hapus_infaq(){
		$id=$this->input->post('infaq_id');
		$gambar=$this->input->post('gambar');
		$data=$this->M_infaq->hapus_infaq($id);
		$path=base_url().'assets/images/'.$gambar;
		delete_files($path);
	    echo $this->session->set_flashdata('msg','success-hapus');
		echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Hapus Infaq Berhasil!</div>');
	    redirect('admin/infaq');
	}

}