<?php
class zakat extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_zakat');
		$this->load->library('upload');
	}


	function index(){
		$id=$this->session->userdata('idadmin');
		// $x['user']=$this->M_zakat->get_pengguna_login($kode);
		$x['data']=$this->M_zakat->get_all_zakat();
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_zakat',$x);
		$this->load->view('admin/v_footer');
	}

	function simpan_zakat(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name']))
	    {
	        if ($this->upload->do_upload('filefoto'))
	        {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
				// $user_id=strip_tags($this->input->post('zakat_user_id'));
				// $tanggal=strip_tags($this->input->post('zakat_tanggal'));
		  //      	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
		  //          $tanggal = date('Y-m-d',strtotime($tanggal));
		  //      	}
				$nama=strip_tags($this->input->post('xjudul'));
				$jumlah=strip_tags($this->input->post('xjumlah'));

				$this->M_zakat->simpan_zakat($nama,$jumlah,$gambar);
				echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Tambah Zakat Berhasil!</div>');
				redirect('admin/zakat');
			}else{
	            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Gambar Gagal</div>');
	            redirect('admin/zakat');
	        }
	         
	    }else{
			// $user_id=strip_tags($this->input->post('zakat_user_id'));
			// $tanggal=strip_tags($this->input->post('zakat_tanggal'));
	  //      	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
	  //          $tanggal = date('Y-m-d',strtotime($tanggal));
	  //      	}
			$nama=strip_tags($this->input->post('nama_zakat'));
			$jumlah=strip_tags($this->input->post('jumlah_zakat'));
			$this->M_zakat->simpan_zakat_tanpa_gambar($nama,$jumlah);
			echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Tambah Zakat Berhasil!</div>');
			redirect('admin/zakat');
		}
				
	}

	function update_zakat(){	
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];
                $judul_zakat=$this->input->post('xjudul');
                $jumlah_zakat=$this->input->post('xjumlah');
				$zakat_id=$this->input->post('zakat_id');
				$this->M_zakat->update_zakat($judul_zakat,$jumlah_zakat,$gambar,$zakat_id);
				echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Edit Zakat Berhasil!</div>');
				redirect('admin/zakat');
	        }else{
	            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Gambar Gagal</div>');
	    		redirect('admin/zakat');
	        }
	    }else{
			// $user_id=strip_tags($this->input->post('zakat_user_id'));
			// $tanggal=strip_tags($this->input->post('zakat_tanggal'));
	  //      	if (!empty($tanggal) AND $tanggal!='0000-00-00 00:00:00') {
	  //          $tanggal = date('Y-m-d',strtotime($tanggal));
	  //      	}
			$nama=strip_tags($this->input->post('xjudul'));
			$jumlah=strip_tags($this->input->post('xjumlah'));
			$zakat_id=$this->input->post('zakat_id');
			$this->M_zakat->update_zakat_tanpa_gambar($nama,$jumlah,$zakat_id);
			echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Edit Zakat Berhasil!</div>');
			redirect('admin/zakat');
		}
	}

	function hapus_zakat(){
		$id=$this->input->post('zakat_id');
		$gambar=$this->input->post('gambar');
		$data=$this->M_zakat->hapus_zakat($id);
		$path=base_url().'assets/images/'.$gambar;
		delete_files($path);
		echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Hapus Zakat Berhasil!</div>');
	    redirect('admin/zakat');
	}

}