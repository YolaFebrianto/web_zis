<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_pengunjung');
	}
	function index(){
		if($this->session->userdata('akses')=='1'){
			$x['visitor'] = $this->M_pengunjung->statistik_pengujung();
			$this->load->view('admin/v_header');
			$this->load->view('admin/v_dashboard');
			$this->load->view('admin/v_footer',$x);
		}else{
			redirect('administrator');
		}
	
	}
	
}