<?php
class Setting extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_setting');
	}
	function index(){
		$data['setting'] = $this->M_setting->get();
		$this->load->view('admin/v_header');
		$this->load->view('admin/v_setting_form',$data);
		$this->load->view('admin/v_footer');
	}
	function update(){
		$nama=$this->input->post('nama');
		$email=$this->input->post('email');
		$telepon=$this->input->post('telepon');
		$norek=$this->input->post('norek');
		$harga_emas=$this->input->post('harga_emas');
		$alamat=$this->input->post('alamat');
		$kabupaten=$this->input->post('kabupaten');
		$propinsi=$this->input->post('propinsi');
		$kodepos=$this->input->post('kodepos');
		try {
			$this->M_setting->update($nama,$email,$telepon,$norek,$harga_emas,$alamat,$kabupaten,$propinsi,$kodepos);
			$this->session->set_flashdata('message','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Update Setting Berhasil!</div>');
		} catch (Exception $e) {
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> '.$e->getMessage().'</div>');
		}
		redirect('admin/setting');
	}
	
}