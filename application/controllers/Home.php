<?php
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_tulisan');
		$this->load->model('M_halaman');
		$this->load->model('M_pengunjung');
		$this->load->model('M_login');
		$this->load->model('M_zakat');
		$this->load->model('M_setting');
		$this->load->model('M_donasi');
		$this->load->model('M_infaq');
		$this->load->model('M_bayar');
		$this->load->library('upload');
		$this->M_pengunjung->count_visitor();
		// if($this->session->userdata('masukDepan') !=TRUE){
  //           $url=base_url('login');
  //           redirect($url);
  //       };
	}
	function index(){
        $x['data']=$this->M_zakat->get_all_zakat();
        $x['data2']=$this->M_tulisan->get_berita_home(4);
			// $x['berita']=$this->M_tulisan->get_berita_home();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_home',$x);
		$this->load->view('depan/v_footer');
	}
	function profil()
	{
		$dataUser = @$this->session->userdata('userIdDepan');
		$x['profilData']=$this->M_login->profil($dataUser)->row_array();
		$x['isi']=$this->M_halaman->get_halaman_by_slug('profil-lembaga')->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_profil_lembaga',$x);
		$this->load->view('depan/v_footer');
	}
	function visi_misi()
	{
		$dataUser = @$this->session->userdata('userIdDepan');
		$x['profilData']=$this->M_login->profil($dataUser)->row_array();
		$x['isi']=$this->M_halaman->get_halaman_by_slug('visi-misi')->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_visi_misi',$x);
		$this->load->view('depan/v_footer');
	}
	function program()
	{	$x['data']=$this->M_donasi->get_all_donasi();
		$x['data1']=$this->M_zakat->get_all_zakat();
		$x['data2']=@$this->M_infaq->get_all_infaq();
		$dataUser = @$this->session->userdata('userIdDepan');
		// $x['profilData']=$this->M_login->profil($dataUser)->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_program',$x);
		$this->load->view('depan/v_footer');
	}

	function bayar_zakat()
	{
		if (isset($_POST['zakat'])) {
			$bayar_nilai = $_POST['zakat'];
			$bayar_nama = $_POST['nama'];
			// $bayar_user_id = @$this->session->userdata('userIdDepan');

			if ($bayar_nilai<=0) {
	            $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Nominal Pembayaran Zakat Harus > 0</div>');
	            redirect('home/kalkulator');
			}

			$config['upload_path'] = './assets/bukti/'; //path folder
		    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		    $this->upload->initialize($config);
		    if(!empty($_FILES['bukti']['name']))
		    {
		        if ($this->upload->do_upload('bukti'))
		        {
	                $gbr = $this->upload->data();
	                //Compress Image
	                $config['image_library']='gd2';
	                $config['source_image']='./assets/bukti/'.$gbr['file_name'];
	                $config['create_thumb']= FALSE;
	                $config['maintain_ratio']= FALSE;
	                $config['quality']= '60%';
	                $config['width']= 300;
	                $config['height']= 300;
	                $config['new_image']= './assets/bukti/'.$gbr['file_name'];
	                $this->load->library('image_lib', $config);
	                $this->image_lib->resize();

	                $bayar_bukti=$gbr['file_name'];

					$this->M_bayar->bayar($bayar_nama,$bayar_nilai,$bayar_bukti);
					$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pembayaran Zakat Berhasil!</div>');
					redirect('home/kalkulator');
	            } else {
		            $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Upload Bukti Gagal</div>');
		            redirect('home/kalkulator');
	            }
	        } else {
				$this->M_bayar->bayar_tanpa_gambar($bayar_nama,$bayar_nilai);
				$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pembayaran Zakat Berhasil!</div>');
				redirect('home/kalkulator');
			}
		}
	}

	function kalkulator(){
        $q=$this->M_setting->get();
        $setting=$q->row_array();
        $x['harga_emas']=0;
        if (!empty($setting)) {
            if (!empty(@$setting['harga_emas'])) {
                $x['harga_emas']=@$setting['harga_emas'];
            }
        }        
		$dataUser = @$this->session->userdata('userIdDepan');
		$x['profilData']=$this->M_login->profil($dataUser)->row_array();
		$this->load->view('depan/v_header');
		$this->load->view('depan/v_kalkulator',$x);
		$this->load->view('depan/v_footer');
	}
}